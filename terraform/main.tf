provider "aws" {
 region = "us-east-1"
 access_key = "${var.acceskey}"
 secret_key = "${var.secretkey}"
}

resource "aws_instance" "web" {
 ami  = "ami-04b9e92b5572fa0d1"
 instance_type = "t2.micro"
 key_name = "${var.llavename}"
 security_groups = ["${aws_security_group.allow_tls.name}"]

 tags = {
  Name = "Examen-Deyvi"
 }
}

resource "aws_default_vpc" "default"{
 tags = {
  Name = "Default VPC"
 }
}

resource "aws_security_group" "allow_tls"{
 name = "security_group_trabajo_clase_convariabl--kjhges"
 description = "security_group_ssh_trabajo_clase_convariables"
 vpc_id = "${aws_default_vpc.default.id}"

 ingress {
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
 }

 egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
 }
}